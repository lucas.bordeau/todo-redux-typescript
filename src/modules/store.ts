import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware, { ThunkAction, ThunkDispatch } from "redux-thunk";
import { DefaultRootState } from "react-redux";

import { todoReducer, TodoState } from "../modules/todo";
import { AppUIState, appUIReducer } from "../modules/app-ui";

import { projectReducer, ProjectState } from "./project";
import { ProjectActions } from "./project/const";
import { TodoActions } from "./todo/const";
import { AppUIActions } from "./app-ui/const";

export const rootReducer = combineReducers({
  todos: todoReducer,
  projects: projectReducer,
  appUI: appUIReducer,
});

function configureStore() {
  return createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  );
}

const store = configureStore();

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  RootActions
>;

export type RootActions = TodoActions | ProjectActions | AppUIActions;

declare module "react-redux" {
  export interface DefaultRootState {
    todos: TodoState;
    projects: ProjectState;
    appUI: AppUIState;
  }

  export function useDispatch(): AppDispatch;
}

export type AppDispatch = ThunkDispatch<RootState, any, RootActions>;

export type RootState = DefaultRootState;

export default store;
