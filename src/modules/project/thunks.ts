import {
  addProjectActionCreator,
  deleteProjectActionCreator,
  updateProjectActionCreator,
  addProjectArray,
} from "./actions";
import { AppThunk } from "../store";
import { ProjectRepository } from "../../fake-database/ProjectRepository";
import { Project } from "../../entities";

export const fetchAllProjects = (): AppThunk => {
  return async (dispatch) => {
    try {
      const projectArray = await ProjectRepository.getAll();

      dispatch(addProjectArray(projectArray));
    } catch (error) {
      console.error(error);
      console.error(
        `Cannot fetch all projects, please verify your connection.`
      );
    }
  };
};

export const createProject = (projectToCreate: Project): AppThunk => {
  return async (dispatch) => {
    try {
      const createdProject = await ProjectRepository.create(projectToCreate);

      dispatch(addProjectActionCreator(createdProject));
    } catch (error) {
      console.error(
        `Cannot fetch all projects, please verify your connection.`
      );
      console.error(error);
    }
  };
};

export const updateProject = (projectToUpdate: Project): AppThunk => {
  return async (dispatch) => {
    try {
      const updatedProject = await ProjectRepository.update(projectToUpdate);

      if (!updatedProject) {
        throw new Error(`Could not update Project : ${projectToUpdate.id}`);
      } else {
        dispatch(updateProjectActionCreator(updatedProject));
      }
    } catch (error) {
      console.error(
        `Cannot update project "${projectToUpdate.id}", please verify your connection.`
      );
      console.error(error);
    }
  };
};

export const deleteProject = (projectToDelete: Project): AppThunk => {
  return async (dispatch) => {
    try {
      await ProjectRepository.deleteById(projectToDelete.id);

      dispatch(deleteProjectActionCreator(projectToDelete));
    } catch (error) {
      console.error(error);
      console.error(
        `Cannot delete project : "${projectToDelete.id}", please verify your connection.`
      );
    }
  };
};
