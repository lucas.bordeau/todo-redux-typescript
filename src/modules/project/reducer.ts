import {
  ADD_PROJECT,
  DELETE_PROJECT,
  UPDATE_PROJECT,
  ADD_PROJECT_ARRAY,
  ProjectActions,
} from "./const";
import { Project } from "../../entities";
import { produce } from "immer";

export interface ProjectState {
  projectArray: Project[];
}

const initialState: ProjectState = {
  projectArray: [],
};

export function projectReducer(
  state = initialState,
  action: ProjectActions
): ProjectState {
  switch (action.type) {
    case ADD_PROJECT:
      return produce(state, (draft) => {
        draft.projectArray?.push(action.payload.newProject);
      });
    case ADD_PROJECT_ARRAY:
      return produce(state, (draft) => {
        for (let projectToAdd of action.payload?.projectArray) {
          if (!draft.projectArray?.find((t) => t.id === projectToAdd.id)) {
            draft.projectArray?.push(projectToAdd);
          }
        }
      });
    case UPDATE_PROJECT:
      return produce(state, (draft) => {
        let foundProject = draft.projectArray?.find(
          (t) => t.id === action.payload?.projectToUpdate?.id
        );

        if (foundProject) {
          foundProject = {
            ...action.payload?.projectToUpdate,
          };
        }
      });
    case DELETE_PROJECT:
      return produce(state, (draft) => {
        const foundProjectIndex = draft.projectArray?.findIndex(
          (p) => p.id === action.payload?.projectToDelete?.id
        );

        if (foundProjectIndex) {
          draft.projectArray?.splice(foundProjectIndex, 1);
        }
      });
    default:
      return state;
  }
}
