import { Project } from "../../entities";

export const ADD_PROJECT = "project/ADD";
export const ADD_PROJECT_ARRAY = "project/ADD_ARRAY";

export const UPDATE_PROJECT = "project/UPDATE";
export const DELETE_PROJECT = "project/DELETE";

export interface AddProjectArrayAction {
  type: typeof ADD_PROJECT_ARRAY;
  payload: {
    projectArray: Project[];
  };
}
export interface AddProjectAction {
  type: typeof ADD_PROJECT;
  payload: {
    newProject: Project;
  };
}

export interface UpdateProjectAction {
  type: typeof UPDATE_PROJECT;
  payload: {
    projectToUpdate: Project;
  };
}

export interface DeleteProjectAction {
  type: typeof DELETE_PROJECT;
  payload: {
    projectToDelete: Project;
  };
}

export type ProjectActions =
  | AddProjectAction
  | AddProjectArrayAction
  | UpdateProjectAction
  | DeleteProjectAction;
