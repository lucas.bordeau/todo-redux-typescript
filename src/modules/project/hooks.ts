import { useDispatch, useSelector } from "react-redux";
import { useEffect, useMemo } from "react";
import { fetchAllProjects } from "./thunks";
import { getAllProjects } from "../project/selectors";

export function useAllProjects() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllProjects());
  }, [dispatch]);

  const projects = useSelector(getAllProjects);

  return projects;
}

export function useProject(projectId: string) {
  const projects = useSelector(getAllProjects);

  return useMemo(() => {
    return projects.find((p) => p.id === projectId);
  }, [projectId, projects]);
}
