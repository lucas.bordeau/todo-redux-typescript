import { RootState } from "../store";

export const getAllProjects = (state: RootState) => state.projects.projectArray;
export const getProjectById = (state: RootState, projectId: string) =>
  state.projects.projectArray.find((p) => p.id === projectId);
