import {
  AddProjectAction,
  ADD_PROJECT,
  DeleteProjectAction,
  UpdateProjectAction,
  DELETE_PROJECT,
  UPDATE_PROJECT,
  AddProjectArrayAction,
  ADD_PROJECT_ARRAY,
} from "./const";
import { Project } from "../../entities";

export function addProjectArray(
  projectArray: Project[]
): AddProjectArrayAction {
  return {
    type: ADD_PROJECT_ARRAY,
    payload: {
      projectArray,
    },
  };
}

export function addProjectActionCreator(newProject: Project): AddProjectAction {
  return {
    type: ADD_PROJECT,
    payload: {
      newProject,
    },
  };
}

export function updateProjectActionCreator(
  projectToUpdate: Project
): UpdateProjectAction {
  return {
    type: UPDATE_PROJECT,
    payload: {
      projectToUpdate,
    },
  };
}

export function deleteProjectActionCreator(
  projectToDelete: Project
): DeleteProjectAction {
  return {
    type: DELETE_PROJECT,
    payload: {
      projectToDelete,
    },
  };
}
