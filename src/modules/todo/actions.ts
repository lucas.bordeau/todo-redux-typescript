import {
  ADD_TODO,
  AddTodoAction,
  AddTodoArrayAction,
  ADD_TODO_ARRAY,
  UpdateTodoAction,
  DeleteTodoAction,
  UPDATE_TODO,
  DELETE_TODO,
} from "./const";
import { Todo } from "../../entities";

export function addTodoArray(todoArray: Todo[]): AddTodoArrayAction {
  return {
    type: ADD_TODO_ARRAY,
    payload: {
      todoArray,
    },
  };
}

export function addTodo(newTodo: Todo): AddTodoAction {
  return {
    type: ADD_TODO,
    payload: {
      newTodo,
    },
  };
}

export function updateTodo(todoToUpdate: Todo): UpdateTodoAction {
  return {
    type: UPDATE_TODO,
    payload: {
      todoToUpdate,
    },
  };
}

export function deleteTodo(todoToDelete: Todo): DeleteTodoAction {
  return {
    type: DELETE_TODO,
    payload: {
      todoToDelete,
    },
  };
}
