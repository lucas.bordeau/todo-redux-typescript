import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchAllTodos } from "./thunks";
import { getAllTodos } from "./selectors";
import { Todo } from "../../entities";
import { TodoRepository } from "../../fake-database/TodoRepository";
import { addTodo, updateTodo } from "./actions";
import produce from "immer";

export function useAllTodos() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllTodos());
  }, [dispatch]);

  const todos = useSelector(getAllTodos);

  return todos;
}

export function useCreateTodo(): (newTodo: Todo) => Promise<void> {
  const dispatch = useDispatch();

  return async (newTodo: Todo) => {
    const createdTodo = await TodoRepository.create(newTodo);

    dispatch(addTodo(createdTodo));
  };
}

export function useChangeTodoState(): (
  todo: Todo,
  done: boolean
) => Promise<void> {
  const dispatch = useDispatch();

  return async (todo: Todo, done: boolean) => {
    const todoToUpdate = produce(todo, (draft) => {
      draft.status = done ? "done" : "todo";
    });

    const updatedTodo = await TodoRepository.update(todoToUpdate);

    if (!updatedTodo) {
      throw new Error(`Cannot update todo in database with id : ${todo.id}`);
    }

    dispatch(updateTodo(updatedTodo));
  };
}
