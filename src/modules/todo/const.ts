import { Todo } from "../../entities";

export const ADD_TODO = "todo/ADD_TODO";
export const ADD_TODO_ARRAY = "todo/ADD_ARRAY";

export const UPDATE_TODO = "todo/UPDATE_TODO";
export const DELETE_TODO = "todo/DELETE_TODO";

export interface AddTodoAction {
  type: typeof ADD_TODO;
  payload: {
    newTodo: Todo;
  };
}

export interface UpdateTodoAction {
  type: typeof UPDATE_TODO;
  payload: {
    todoToUpdate: Todo;
  };
}

export interface DeleteTodoAction {
  type: typeof DELETE_TODO;
  payload: {
    todoToDelete: Todo;
  };
}

export interface AddTodoArrayAction {
  type: typeof ADD_TODO_ARRAY;
  payload: {
    todoArray: Todo[];
  };
}

export type TodoActions =
  | AddTodoAction
  | AddTodoArrayAction
  | UpdateTodoAction
  | DeleteTodoAction;
