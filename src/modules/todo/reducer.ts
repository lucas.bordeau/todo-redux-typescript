import { Todo } from "../../entities";
import {
  ADD_TODO,
  TodoActions,
  UPDATE_TODO,
  DELETE_TODO,
  ADD_TODO_ARRAY,
} from "./const";
import { produce } from "immer";

export interface TodoState {
  todoArray: Todo[];
}

const initialState: TodoState = {
  todoArray: [],
};

export function todoReducer(
  state = initialState,
  action: TodoActions
): TodoState {
  switch (action.type) {
    case ADD_TODO:
      return produce(state, (draft) => {
        draft.todoArray.push(action.payload.newTodo);
      });
    case ADD_TODO_ARRAY:
      return produce(state, (draft) => {
        for (const todoToAdd of action.payload.todoArray) {
          if (!draft.todoArray.find((t) => t.id === todoToAdd.id)) {
            draft.todoArray.push(todoToAdd);
          }
        }
      });
    case UPDATE_TODO:
      return produce(state, (draft) => {
        const foundTodoIndex = draft.todoArray.findIndex(
          (t) => t.id === action.payload.todoToUpdate.id
        );

        if (foundTodoIndex > -1) {
          draft.todoArray[foundTodoIndex] = action.payload.todoToUpdate;
        }
      });
    case DELETE_TODO:
      return produce(state, (draft) => {
        const foundTodoIndex = draft.todoArray.findIndex(
          (t) => t.id === action.payload.todoToDelete.id
        );

        if (foundTodoIndex > -1) {
          draft.todoArray.splice(foundTodoIndex, 1);
        }
      });
    default:
      return state;
  }
}
