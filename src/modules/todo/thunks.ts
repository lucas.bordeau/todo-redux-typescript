import { TodoRepository } from "../../fake-database/TodoRepository";
import { TodoActions } from "./const";
import { Dispatch } from "redux";
import { addTodoArray, updateTodo, deleteTodo, addTodo } from "./actions";
import { Todo } from "../../entities";

export const fetchAllTodos = () => {
  return async (dispatch: Dispatch<TodoActions>) => {
    try {
      const todoArray = await TodoRepository.getAll();

      dispatch(addTodoArray(todoArray));
    } catch (error) {
      console.error(error);
      console.error(`Cannot fetch all todos, please verify your connection.`);
    }
  };
};

export const createTodoInDB = (todoToCreate: Todo) => {
  return async (dispatch: Dispatch<TodoActions>) => {
    try {
      const createdTodo = await TodoRepository.create(todoToCreate);

      dispatch(addTodo(createdTodo));
    } catch (error) {
      console.error(`Cannot fetch all todos, please verify your connection.`);
      console.error(error);
    }
  };
};

export const updateTodoInDB = (todoToUpdate: Todo) => {
  return async (dispatch: Dispatch<TodoActions>) => {
    try {
      const updatedTodo = await TodoRepository.update(todoToUpdate);

      if (!updatedTodo) {
        throw new Error(`Could not update Todo : ${todoToUpdate.id}`);
      } else {
        dispatch(updateTodo(updatedTodo));
      }
    } catch (error) {
      console.error(
        `Cannot update todo "${todoToUpdate.id}", please verify your connection.`
      );
      console.error(error);
    }
  };
};

export const deleteTodoInDB = (todoToDelete: Todo) => {
  return async (dispatch: Dispatch<TodoActions>) => {
    try {
      await TodoRepository.deleteById(todoToDelete.id);

      dispatch(deleteTodo(todoToDelete));
    } catch (error) {
      console.error(error);
      console.error(
        `Cannot delete todo : "${todoToDelete.id}", please verify your connection.`
      );
    }
  };
};
