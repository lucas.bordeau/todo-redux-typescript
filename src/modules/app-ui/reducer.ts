import {
  AppUIActions,
  TRIGGER_ERROR,
  SET_LOADING_STATE,
  SET_APP_SNACKBAR_OPEN,
} from "./const";
import { AppSnackbarLevel } from "../../types/AppSnackbarLevel";
import produce from "immer";

export interface AppUIState {
  loading: boolean;
  appSnackbar: {
    open: boolean;
    level: AppSnackbarLevel | null;
    text: string;
  };
}

const initialState: AppUIState = {
  loading: false,
  appSnackbar: {
    open: false,
    level: null,
    text: "",
  },
};

export function appUIReducer(
  state = initialState,
  action: AppUIActions
): AppUIState {
  switch (action.type) {
    case TRIGGER_ERROR: {
      return produce(state, (draft) => {
        draft.appSnackbar.level = "error";
        draft.appSnackbar.open = true;
        draft.appSnackbar.text = action.payload.errorMessage;
      });
    }
    case SET_APP_SNACKBAR_OPEN: {
      return produce(state, (draft) => {
        draft.appSnackbar.open = action.payload.open;
      });
    }
    case SET_LOADING_STATE: {
      return produce(state, (draft) => {
        draft.loading = action.payload.loading;
      });
    }
    default:
      return state;
  }
}
