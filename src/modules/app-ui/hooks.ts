import { useDispatch, useSelector } from "react-redux";
import { setAppSnackbarOpen, triggerError } from "./actions";

export function useShowError(): (errorMessage: string) => void {
  const dispatch = useDispatch();

  return (errorMessage: string) => {
    dispatch(triggerError(errorMessage));
  };
}

export function useAppSnackbarOpen(): [boolean, (open: boolean) => void] {
  const dispatch = useDispatch();

  const appSnackbarOpen = useSelector((state) => state.appUI.appSnackbar.open);

  function setOpen(open: boolean) {
    dispatch(setAppSnackbarOpen(open));
  }

  return [appSnackbarOpen, setOpen];
}
