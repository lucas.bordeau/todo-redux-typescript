export const TRIGGER_ERROR = "app-ui/TRIGGER_ERROR";
export const SET_LOADING_STATE = "app-ui/SET_LOADING_STATE";
export const SET_APP_SNACKBAR_OPEN = "app-ui/SET_APP_SNACKBAR_OPEN";

export interface TriggerErrorAppUIAction {
  type: typeof TRIGGER_ERROR;
  payload: {
    errorMessage: string;
  };
}

export interface SetLoadingStateAppUIAction {
  type: typeof SET_LOADING_STATE;
  payload: {
    loading: boolean;
  };
}

export interface SetAppSnackbarOpenAppUIAction {
  type: typeof SET_APP_SNACKBAR_OPEN;
  payload: {
    open: boolean;
  };
}

export type AppUIActions =
  | TriggerErrorAppUIAction
  | SetLoadingStateAppUIAction
  | SetAppSnackbarOpenAppUIAction;
