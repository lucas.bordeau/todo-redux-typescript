import {
  TriggerErrorAppUIAction,
  TRIGGER_ERROR,
  SetLoadingStateAppUIAction,
  SET_LOADING_STATE,
  SetAppSnackbarOpenAppUIAction,
  SET_APP_SNACKBAR_OPEN,
} from "./const";

export function triggerError(errorMessage: string): TriggerErrorAppUIAction {
  return {
    type: TRIGGER_ERROR,
    payload: {
      errorMessage,
    },
  };
}

export function setLoadingState(loading: boolean): SetLoadingStateAppUIAction {
  return {
    type: SET_LOADING_STATE,
    payload: {
      loading,
    },
  };
}

export function setAppSnackbarOpen(
  open: boolean
): SetAppSnackbarOpenAppUIAction {
  return {
    type: SET_APP_SNACKBAR_OPEN,
    payload: {
      open,
    },
  };
}
