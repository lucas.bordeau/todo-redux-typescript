export type SelectOption = {
  key: string;
  label?: string;
};
