export type AppSnackbarLevel = "error" | "info" | "success" | "warning";
