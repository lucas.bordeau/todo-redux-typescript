import { Todo } from "./Todo";

export interface Project {
  id: string;
  name?: string;
  todoArray?: Todo[];
}
