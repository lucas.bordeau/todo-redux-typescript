import { TodoStatus } from "../types/TodoStatus";

export interface Todo {
  id: string;
  projectId: string;
  description?: string;
  createdAt: Date;
  dueDate?: Date;
  status: TodoStatus;
}
