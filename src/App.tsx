import React, { useEffect } from "react";
import "./App.css";
import { ApplicationLayout } from "./components/AppLayout/ApplicationLayout";

import { useHistory } from "react-router-dom";
import store from "./modules/store";
import { Provider } from "react-redux";

function App() {
  const history = useHistory();

  useEffect(() => {
    if (history.location.pathname === "/") {
      history.push("/all-todos");
    }
  }, [history]);

  return (
    <Provider store={store}>
      <ApplicationLayout />
    </Provider>
  );
}

export default App;
