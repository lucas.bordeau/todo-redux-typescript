import React, { useState } from "react";
import { Todo, Project } from "../../entities";
import { SimpleDialog } from "../Reusable/SimpleDialog";
import { TextEdit } from "../Reusable/TextEdit";
import { SelectEntity } from "../Reusable/SelectEntity";
import { useAllProjects } from "../../modules/project/hooks";
import { makeStyles } from "@material-ui/core";

const PROJECT_ID_ERROR_MESSAGE = "Please select a project";

const useStyles = makeStyles({
  input: {
    marginTop: 20,
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
  },
  dialog: {
    minWidth: 300,
  },
});

type Props = {
  mode: "create" | "edit";
  todo?: Todo | null;
  open: boolean;
  onValidate: (todo: Todo) => void;
  onCancel: () => void;
};

export function CreateEditTodoDialog({
  mode,
  todo,
  open,
  onValidate,
  onCancel,
}: Props) {
  const classes = useStyles();

  const [description, setDescription] = useState("");
  const [projectId, setProjectId] = useState<string | null>(null);
  const [errored, setErrored] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const projects = useAllProjects();

  function cleanState() {
    setDescription("");
    setProjectId(null);
    setErrored(false);
    setErrorMessage(null);
  }

  function handleValidate() {
    let newTodo = { ...todo } as Todo;

    newTodo.description = description;

    if (!projectId) {
      setErrored(true);
      setErrorMessage(PROJECT_ID_ERROR_MESSAGE);
    } else {
      newTodo.projectId = projectId;

      onValidate(newTodo);
      cleanState();
    }
  }

  function handleCancel() {
    onCancel();
    cleanState();
  }

  function handleDescriptionChange(newDescription: string) {
    setDescription(newDescription);
  }

  function handleProjectChange(newProject: Project | null) {
    setProjectId(newProject?.id ?? null);

    if (newProject?.id && errored) {
      setErrored(false);
    }
  }

  return (
    <SimpleDialog
      open={open}
      onCancel={handleCancel}
      onValidate={handleValidate}
      title={mode === "create" ? "Create Todo" : "Edit Todo"}
    >
      <div className={classes.dialog}>
        <div className={classes.input}>
          <TextEdit
            label="Description"
            onChange={handleDescriptionChange}
            value={description}
            multiline
          />
        </div>
        <div className={classes.input}>
          <SelectEntity<Project>
            entities={projects}
            label="Project"
            onSelect={handleProjectChange}
            optionLabelField="name"
            selectedEntityId={projectId}
            errored={errored}
            errorMessage={errorMessage}
          />
        </div>
      </div>
    </SimpleDialog>
  );
}
