import React from "react";

import { TodoList } from "./TodoList";

import { MediumSizedPage } from "../Reusable/MediumSizedPage";
import { TodoListHeader } from "./TodoListHeader";

export function TodoScreen() {
  return (
    <MediumSizedPage>
      <TodoListHeader />
      <TodoList />
    </MediumSizedPage>
  );
}
