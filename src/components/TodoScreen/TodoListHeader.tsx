import React, { useState } from "react";
import { ScreenTitle } from "../Reusable/ScreenTitle";
import { Button, makeStyles } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { CreateEditTodoDialog } from "./CreateEditTodoDialog";
import { Todo } from "../../entities";
import { useCreateTodo } from "../../modules/todo/hooks";
import { useShowError } from "../../modules/app-ui/hooks";
import { getErrorMessage } from "../../utils/getErrorMessage";

const useStyles = makeStyles({
  header: {
    display: "flex",
    flexDirection: "row",
    marginTop: 20,
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
  },
});

export function TodoListHeader() {
  const classes = useStyles();

  const [dialogOpen, setDialogOpen] = useState(false);
  const showError = useShowError();

  const createTodo = useCreateTodo();

  function handleCreateTodoClick() {
    setDialogOpen(true);
  }

  function handleCreateDialogCancel() {
    setDialogOpen(false);
  }

  async function handleCreateDialogValidate(newTodo: Todo) {
    setDialogOpen(false);

    try {
      await createTodo(newTodo);
    } catch (error) {
      const errorMessage = `Error during creation of todo : ${getErrorMessage(
        error
      )}`;

      showError(errorMessage);
      console.error(error);
    }
  }

  return (
    <div className={classes.header}>
      <ScreenTitle variant="left">Todos</ScreenTitle>
      <Button
        variant="contained"
        color="primary"
        onClick={handleCreateTodoClick}
      >
        Ajouter <AddIcon />
      </Button>
      <CreateEditTodoDialog
        mode="create"
        open={dialogOpen}
        onValidate={handleCreateDialogValidate}
        onCancel={handleCreateDialogCancel}
      />
    </div>
  );
}
