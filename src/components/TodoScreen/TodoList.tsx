import React from "react";
import { List, makeStyles, ListItem, ListItemText } from "@material-ui/core";
import { useAllTodos } from "../../modules/todo/hooks";
import { TodoListItem } from "./TodoListItem";

const useStyles = makeStyles((theme) => ({
  list: {
    backgroundColor: theme.palette.background.paper,
    width: "100%",
    minWidth: "50px",
  },
  noTodo: {
    textAlign: "center",
  },
  listItem: {
    borderBottom: "#80808021 1px solid",
  },
  listItemDone: {
    textDecoration: "line-through",
  },
}));

export function TodoList() {
  const classes = useStyles();

  const todos = useAllTodos();

  return (
    <List dense className={classes.list}>
      {todos.length > 0 ? (
        todos.map((todo, index) => (
          <TodoListItem key={todo.id} todo={todo} index={index} />
        ))
      ) : (
        <ListItem>
          <ListItemText className={classes.noTodo}>
            Nothing to do here.
          </ListItemText>
        </ListItem>
      )}
    </List>
  );
}
