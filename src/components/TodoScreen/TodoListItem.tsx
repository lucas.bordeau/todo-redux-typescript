import React, { useState } from "react";
import clsx from "clsx";

import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox,
  makeStyles,
} from "@material-ui/core";
import { useProject } from "../../modules/project/hooks";
import { Todo } from "../../entities";
import { useChangeTodoState } from "../../modules/todo/hooks";
import { useShowError } from "../../modules/app-ui/hooks";
import { getErrorMessage } from "../../utils/getErrorMessage";

const useStyles = makeStyles((theme) => ({
  listItem: {
    borderBottom: "#80808021 1px solid",
  },
  listItemDone: {
    textDecoration: "line-through",
    fontSize: 20,
    "&:hover": {
      textDecoration: "line-through",
    },
  },
}));

type Props = {
  todo: Todo;
  index: number;
};

export function TodoListItem({ todo, index }: Props) {
  const [checked, setChecked] = useState(todo.status === "done");

  const showError = useShowError();

  const project = useProject(todo.projectId);

  const changeTodoState = useChangeTodoState();

  async function handleCheckChange(
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) {
    setChecked(checked);

    try {
      await changeTodoState(todo, checked);
    } catch (error) {
      showError(`Error : Cannot update todo : ${getErrorMessage(error)}`);
      console.error(error);
      setChecked(!checked);
    }
  }

  const labelId = `checkbox-list-secondary-label-${todo.id}`;

  const classes = useStyles();

  return (
    <ListItem
      key={todo.id}
      button
      className={clsx({
        [classes.listItemDone]: checked,
      })}
    >
      <ListItemText id={labelId} secondary={<span>{project?.name ?? ""}</span>}>
        {todo.description}
      </ListItemText>
      <ListItemSecondaryAction>
        <Checkbox
          edge="end"
          inputProps={{ "aria-labelledby": labelId }}
          onChange={handleCheckChange}
          checked={checked}
        />
      </ListItemSecondaryAction>
    </ListItem>
  );
}
