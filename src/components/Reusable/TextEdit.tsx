import React, { useState } from "react";
import { TextField } from "@material-ui/core";

type Props = {
  label: string;
  value: string;
  onChange: (newValue: string) => void;
  multiline?: boolean;
};

export function TextEdit({ label, value, onChange, multiline }: Props) {
  const [currentValue, setCurrentValue] = useState(value);

  function handleChange(
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) {
    const newValue = event.target.value;

    setCurrentValue(newValue);
    onChange(newValue);
  }

  return (
    <TextField
      label={label}
      value={currentValue}
      onChange={handleChange}
      fullWidth
      variant="outlined"
      multiline={multiline}
    />
  );
}
