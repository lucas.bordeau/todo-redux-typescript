import React from "react";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles({
  header: {
    paddingBottom: 0,
  },
});

type SimpleDialogProps = {
  title: string;
  open: boolean;
  onCancel: () => void;
  onValidate: () => void;
};

export function SimpleDialog({
  title,
  open,
  onCancel,
  onValidate,
  children,
}: React.PropsWithChildren<SimpleDialogProps>) {
  const classes = useStyles();

  function handleCancel() {
    onCancel();
  }

  function handleValidate() {
    onValidate();
  }

  return (
    <>
      <Dialog key="dialog" open={open} onClose={handleCancel}>
        <DialogTitle className={classes.header}>{title}</DialogTitle>
        <DialogContent>{children}</DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleValidate}
            color="primary"
            variant="contained"
            autoFocus
          >
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
