import React from "react";
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  titleCentered: {
    textAlign: "center",
    marginTop: 30,
    marginBottom: 30,
  },
  titleLeft: {
    textAlign: "left",
  },
});

type ScreenTitleProps = {
  variant: "centered" | "left";
};

export function ScreenTitle({
  variant,
  children,
}: React.PropsWithChildren<ScreenTitleProps>) {
  const classes = useStyles();

  const typographyVariant = variant === "centered" ? "h4" : "h5";
  const titleClassName =
    variant === "centered" ? classes.titleCentered : classes.titleLeft;

  return (
    <Typography variant={typographyVariant} className={titleClassName}>
      {children}
    </Typography>
  );
}
