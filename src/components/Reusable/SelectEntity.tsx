import React, { useState, useEffect } from "react";
import { TextField } from "@material-ui/core";
import { Autocomplete, AutocompleteRenderInputParams } from "@material-ui/lab";
import { ExtractStringKeys } from "../../types/ExtractStringKeys";
import { SelectOption } from "../../types/SelectOption";
import { SelectOptionManager } from "../../utils/SelectOptionManager";
import { Entity } from "../../types/Entity";

const DEFAULT_ERROR_MESSAGE = "Please select a value";

type Props<T> = {
  label: string;
  optionLabelField: ExtractStringKeys<T>;
  selectedEntityId: string | null;
  entities: T[];
  onSelect: (selectedEntity: T | null) => void;
  errored?: boolean;
  errorMessage?: string | null;
};

export function SelectEntity<T extends Entity>({
  label,
  optionLabelField,
  selectedEntityId,
  entities,
  onSelect,
  errored,
  errorMessage,
}: Props<T>) {
  const [selectedOption, setSelectedOption] = useState<SelectOption | null>(
    null
  );
  const [options, setOptions] = useState<SelectOption[]>([]);

  useEffect(() => {
    const options = SelectOptionManager.turnEntityArrayIntoOptionArray(
      entities,
      optionLabelField
    );

    setOptions(options);
  }, [entities, optionLabelField]);

  useEffect(() => {
    const selectedOption = options.find((o) => o.key === selectedEntityId);

    setSelectedOption(selectedOption ?? null);
  }, [options, selectedEntityId]);

  function handleSelect(
    _event: React.ChangeEvent<{}>,
    selectedOption: SelectOption | null
  ) {
    const selectedEntity = entities.find((e) => e.id === selectedOption?.key);

    setSelectedOption(selectedOption);

    onSelect(selectedEntity ?? null);
  }

  function renderSelectInput(params: AutocompleteRenderInputParams) {
    const helperText = errored ? errorMessage || DEFAULT_ERROR_MESSAGE : null;

    return (
      <TextField
        {...params}
        label={label}
        variant="outlined"
        error={!!errored}
        helperText={helperText}
        fullWidth
      />
    );
  }

  return (
    <Autocomplete
      options={options}
      getOptionLabel={SelectOptionManager.getOptionLabel}
      value={selectedOption}
      getOptionSelected={SelectOptionManager.compareSelectedOption}
      onChange={handleSelect}
      renderInput={renderSelectInput}
      size="small"
      multiple={false}
      clearOnEscape
      autoHighlight
      openOnFocus
      fullWidth
    />
  );
}
