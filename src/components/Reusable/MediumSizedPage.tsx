import React from "react";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  container: {
    maxWidth: 768,
    width: "100%",
  },
});

export function MediumSizedPage({ children }: React.PropsWithChildren<{}>) {
  const classes = useStyles();

  return <div className={classes.container}>{children}</div>;
}
