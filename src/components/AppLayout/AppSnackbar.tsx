import React from "react";
import { Snackbar, SnackbarCloseReason } from "@material-ui/core";

import { Alert } from "@material-ui/lab";
import { useSelector } from "react-redux";
import { useAppSnackbarOpen } from "../../modules/app-ui/hooks";

export function AppSnackbar() {
  const level = useSelector((state) => state.appUI.appSnackbar.level);
  const text = useSelector((state) => state.appUI.appSnackbar.text);

  const [open, setOpen] = useAppSnackbarOpen();

  function handleClose(
    _event: React.SyntheticEvent<any, Event>,
    reason?: SnackbarCloseReason
  ) {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  }

  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
    >
      <Alert
        elevation={6}
        variant="filled"
        onClose={handleClose}
        severity={level || "info"}
      >
        <span>{text}</span>
      </Alert>
    </Snackbar>
  );
}
