import React from "react";
import { makeStyles, Toolbar } from "@material-ui/core";
import { Switch, Route } from "react-router-dom";
import { TodoScreen } from "../TodoScreen/TodoScreen";
import { ProjectScreen } from "../ProjectScreen/ProjectScreen";
import { AppSnackbar } from "./AppSnackbar";

const useStyles = makeStyles({
  container: {
    padding: "0 15px 15px 15px",
    width: "100%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
});

export function MainContainer() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Toolbar />
      <Switch>
        <Route path="/all-todos">
          <TodoScreen />
        </Route>
        <Route path="/projects">
          <ProjectScreen />
        </Route>
      </Switch>
      <AppSnackbar />
    </div>
  );
}
