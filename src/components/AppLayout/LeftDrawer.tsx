import React from "react";

import { Toolbar, Drawer, List, makeStyles } from "@material-ui/core";

import ListIcon from "@material-ui/icons/List";
import FolderOpenIcon from "@material-ui/icons/FolderOpen";

import { LeftDrawerLinkItem } from "./LeftDrawerLinkItem";

export const LEFT_DRAWER_WIDTH = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: LEFT_DRAWER_WIDTH,
    flexShrink: 0,
  },
  drawerPaper: {
    width: LEFT_DRAWER_WIDTH,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  link: {
    "&:visited": {},
  },
}));

export function LeftDrawer() {
  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>
          <LeftDrawerLinkItem
            key="all-todos"
            path="/all-todos"
            text="All todos"
            icon={<ListIcon />}
          />
          <LeftDrawerLinkItem
            key="projects"
            path="/projects"
            text="Projects"
            icon={<FolderOpenIcon />}
          />
        </List>
      </div>
    </Drawer>
  );
}
