import React from "react";
import { ApplicationScreenPath } from "../../types/ApplicationScreenPath";
import { ListItem, ListItemIcon, ListItemText } from "@material-ui/core";

import { useHistory } from "react-router-dom";

type LeftDrawerLinkItemProps = {
  path: ApplicationScreenPath;
  icon?: JSX.Element;
  text: string;
};

export function LeftDrawerLinkItem({
  icon,
  path,
  text,
}: LeftDrawerLinkItemProps) {
  const history = useHistory();

  function handleClick() {
    history.push(path);
  }

  return (
    <ListItem button key="all-todos" onClick={handleClick}>
      {icon && <ListItemIcon>{icon}</ListItemIcon>}
      <ListItemText>{text}</ListItemText>
    </ListItem>
  );
}
