import React from "react";

import { Toolbar, AppBar, Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));

export function ApplicationNavBar() {
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <Typography variant="h6">Todo Redux + TypeScript Demo</Typography>
      </Toolbar>
    </AppBar>
  );
}
