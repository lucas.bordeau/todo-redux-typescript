import React from "react";

import { LeftDrawer } from "./LeftDrawer";
import { ApplicationNavBar } from "./ApplicationNavBar";
import { makeStyles, CssBaseline } from "@material-ui/core";
import { MainContainer } from "./MainContainer";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
}));

export function ApplicationLayout() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <ApplicationNavBar />
      <LeftDrawer />
      <MainContainer />
    </div>
  );
}
