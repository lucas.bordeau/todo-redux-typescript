import { waitForMilliseconds } from "../utils/waitForMilliseconds";
import { database } from "./db";
import { Project } from "../entities";
import { v4 } from "uuid";

export class ProjectRepository {
  static async getAll() {
    await waitForMilliseconds(500);

    return database.projects;
  }

  static async findById(projectId: string) {
    await waitForMilliseconds(500);

    return database.projects.find((p) => p.id === projectId);
  }

  static async update(project: Project) {
    await waitForMilliseconds(500);

    let foundProjectIndex = database.projects.findIndex(
      (p) => p.id === project.id
    );

    if (foundProjectIndex > -1) {
      database.projects[foundProjectIndex] = project;
    } else {
      throw new Error(`Cannot update project with id in DB : ${project.id}`);
    }

    return database.projects[foundProjectIndex];
  }

  static async create(projectToCreate: Project) {
    await waitForMilliseconds(500);

    projectToCreate = {
      ...projectToCreate,
      id: v4(),
    };

    database.projects.push(projectToCreate);

    return projectToCreate;
  }

  static async deleteById(projectId: string) {
    await waitForMilliseconds(500);

    const foundProjectIndex = database.projects.findIndex(
      (t) => t.id === projectId
    );

    if (foundProjectIndex > -1) {
      database.projects.splice(foundProjectIndex, 1);
    }
  }
}
