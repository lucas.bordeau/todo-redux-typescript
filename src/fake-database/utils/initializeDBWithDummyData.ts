import { Project, Todo } from "../../entities";
import { v4 } from "uuid";
import moment from "moment";
import { FakeDatabase } from "../db";

export function initializeDBWithDummyData() {
  let database: FakeDatabase = {
    projects: [],
    todos: [],
  };

  let workProject: Project = {
    id: v4(),
    name: "Work",
  };

  let financesProject: Project = {
    id: v4(),
    name: "Finances",
  };

  let homeProject: Project = {
    id: v4(),
    name: "Home",
  };

  let workTodoArray: Todo[] = [
    {
      id: v4(),
      projectId: workProject.id,
      createdAt: moment()
        .subtract(Math.random() * 500, "hours")
        .toDate(),
      status: "todo",
      description: "Get a new pen, a good one",
    },
    {
      id: v4(),
      projectId: workProject.id,
      createdAt: moment()
        .subtract(Math.random() * 500, "hours")
        .toDate(),
      status: "todo",
      description: "Get a fancy suit for the annual gala",
    },
  ];

  let financesTodoArray: Todo[] = [
    {
      id: v4(),
      projectId: financesProject.id,
      createdAt: moment()
        .subtract(Math.random() * 500, "hours")
        .toDate(),
      status: "done",
      description: "Finish Rich Dad Poor Dad",
    },
  ];

  let homeTodoArray: Todo[] = [
    {
      id: v4(),
      projectId: homeProject.id,
      createdAt: moment()
        .subtract(Math.random() * 500, "hours")
        .toDate(),
      status: "todo",
      description: "Buy a new carpet",
    },
    {
      id: v4(),
      projectId: homeProject.id,
      createdAt: moment()
        .subtract(Math.random() * 500, "hours")
        .toDate(),
      status: "todo",
      description: "Look for a solar panel company",
    },
  ];

  database.todos = ([] as Todo[]).concat(
    ...workTodoArray,
    ...financesTodoArray,
    ...homeTodoArray
  );

  workProject.todoArray = workTodoArray;
  financesProject.todoArray = financesTodoArray;
  homeProject.todoArray = homeTodoArray;

  database.projects = [workProject, homeProject, financesProject];

  return database;
}
