import { Project, Todo } from "../entities";
import { initializeDBWithDummyData } from "./utils/initializeDBWithDummyData";

export interface FakeDatabase {
  todos: Todo[];
  projects: Project[];
}

export let database = initializeDBWithDummyData();
