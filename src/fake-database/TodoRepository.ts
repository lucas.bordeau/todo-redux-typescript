import { waitForMilliseconds } from "../utils/waitForMilliseconds";
import { database } from "./db";
import { Todo } from "../entities";
import { v4 } from "uuid";

export class TodoRepository {
  static async getAll() {
    await waitForMilliseconds(500);

    return database.todos;
  }

  static async findById(todoId: string) {
    await waitForMilliseconds(500);

    return database.todos.find((t) => t.id === todoId);
  }

  static async update(todo: Todo) {
    await waitForMilliseconds(500);

    let foundTodoIndex = database.todos.findIndex((t) => t.id === todo.id);

    if (foundTodoIndex > -1) {
      database.todos[foundTodoIndex] = todo;
    } else {
      throw new Error(`Cannot update todo with id in DB : ${todo.id}`);
    }

    return database.todos[foundTodoIndex];
  }

  static async create(todoToCreate: Todo) {
    await waitForMilliseconds(500);

    todoToCreate = {
      ...todoToCreate,
      id: v4(),
      createdAt: new Date(),
      status: "todo",
    };

    database.todos.push(todoToCreate);

    return todoToCreate;
  }

  static async deleteById(todoId: string) {
    await waitForMilliseconds(500);

    const foundTodoIndex = database.todos.findIndex((t) => t.id === todoId);

    if (foundTodoIndex > -1) {
      database.todos.splice(foundTodoIndex, 1);
    }
  }
}
