import { SelectOption } from "../types/SelectOption";
import { Entity } from "../types/Entity";
import { ExtractStringKeys } from "../types/ExtractStringKeys";

export class SelectOptionManager {
  static compareSelectedOption(
    comparisonOption: SelectOption,
    selectedOption: SelectOption
  ) {
    if (!comparisonOption || !selectedOption) return false;

    return comparisonOption.key === selectedOption.key;
  }

  static getOptionLabel(option: SelectOption) {
    if (!option) return "";

    return option.label || "";
  }

  static turnEntityArrayIntoOptionArray<T extends Entity>(
    entityArray: T[],
    labelField: ExtractStringKeys<T>
  ): SelectOption[] {
    return entityArray.map((entity) => ({
      key: entity.id,
      label: (entity[labelField] as unknown) as string | undefined,
    }));
  }
}
